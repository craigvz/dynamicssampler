//
//  MainViewController.m
//  DynamicsSampler
//
//  Created by Craig VanderZwaag on 10/9/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "MainViewController.h"
#import "GravityViewController.h"
#import "AngularViewController.h"
#import "CollisionsViewController.h"
#import "AttachmentsViewController.h"
#import "TumbleViewController.h"

@interface MainViewController ()

@property (nonatomic, strong) NSArray *array;

@end

@implementation MainViewController

@synthesize array = _array;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _array = @[@"Gravity", @"Angular", @"Collisions", @"Attachment", @"Tumble"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [_array count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *cellValue = [_array objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    
    // Configure the cell...
    
    return cell;
}



#pragma mark - Navigation

 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     
     
     if (indexPath.row == 0) {
         
         GravityViewController *gravityVC = [[GravityViewController alloc] init];
         gravityVC.navigationItem.title = @"Gravity";
         [self.navigationController pushViewController:gravityVC animated:NO];
         
     }
     
     if (indexPath.row == 1) {
         
         AngularViewController *angularVC = [[AngularViewController alloc] init];
         angularVC.navigationItem.title = @"Angular";
         [self.navigationController pushViewController:angularVC animated:NO];
     }
     
     if (indexPath.row == 2) {
         
         CollisionsViewController *collisionsVC = [[CollisionsViewController alloc] init];
         collisionsVC.navigationItem.title = @"Collisions";
         [self.navigationController pushViewController:collisionsVC animated:NO];
     }
     
     if (indexPath.row == 3) {
         
         AttachmentsViewController *attachmentsVC = [[AttachmentsViewController alloc] init];
         attachmentsVC.navigationItem.title = @"Attachment";
         [self.navigationController pushViewController:attachmentsVC animated:NO];
     }
     
     if (indexPath.row == 4) {
         
         TumbleViewController *tumbleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tumble"];
         tumbleVC.navigationItem.title = @"Tumble";
         [self.navigationController pushViewController:tumbleVC animated:YES];
     }
     
 
 }
 



@end
