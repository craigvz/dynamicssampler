//
//  AppDelegate.h
//  DynamicsSampler
//
//  Created by Craig VanderZwaag on 10/9/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
