//
//  main.m
//  DynamicsSampler
//
//  Created by Craig VanderZwaag on 10/9/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
